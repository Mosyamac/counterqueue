

shinyUI(fluidPage(useShinyjs(), 
                  
  navbarPage(theme = shinytheme("flatly"), id = "myWholePage",
             title="Queue Forecast",
             
             tabPanel(title = "Assumptions", icon=icon("database"),
                      fluidRow(
                        column(width = 3,
                               h3("Models Section"),
                               shiny::sliderInput(inputId = "models_input",
                                                   label = "Models",
                                                   value = 1500,
                                                   min = 0, max = 5000,
                                                   step = 100, width = "100%"),
                               shiny::selectizeInput(inputId = "startMonthName", label = "Current Month",
                                                     choices = names(.GlobalEnv$mnths_grid),
                                                     selected = names(.GlobalEnv$mnths_grid)[lubridate::month(Sys.Date())]),
                               shiny::radioButtons(inputId = "adv_models_settings", label = "Models Settings",
                                                   choices = c("Default","Advanced"), selected = "Default",
                                                   inline = T, width = "100%"),
                               shiny::br(),
                                      rHandsontableOutput(outputId = "custom_models"),
                               shiny::br(),
                               shiny::sliderInput(inputId = "init_q",
                                                  label = "Initial Queue Size",
                                                  value = 100,
                                                  min = 0, max = 1000,
                                                  step = 10, width = "100%"),
                               shiny::sliderInput(inputId = "init_p",
                                                  label = "Initial In-progress Size",
                                                  value = 50,
                                                  min = 0, max = 100,
                                                  step = 10, width = "100%"),
                               ),
                        column(width = 1),
                        column(width = 3,
                               h3("Labor Section"),
                               shiny::sliderInput(inputId = "labor_cap_avg",
                                                  label = "Average w/days per 1 model",
                                                  value = 10,
                                                  min = 1, max = 30,
                                                  step = 1, width = "100%"),
                               shiny::sliderInput(inputId = "people",
                                                  label = "Workers disposable",
                                                  value = 50,
                                                  min = 30, max = 100,
                                                  step = 5, width = "100%"),
                               shiny::radioButtons(inputId = "adv_labor_settings", label = "Labor Settings",
                                                   choices = c("Default","Advanced"), selected = "Default",
                                                   inline = T, width = "100%"),
                               shinyjs::hidden(
                               shiny::sliderInput(inputId = "labor_cap_sd",
                                                  label = "St.dev. in w/days per 1 model",
                                                  value = 2,
                                                  min = 1, max = 10,
                                                  step = 1, width = "100%")),
                               shiny::br(),
                               rHandsontableOutput(outputId = "custom_labor"),
                               ),
                        column(width = 3, 
                               shiny::numericInput(inputId = "n_sim", label = "Simulations",
                                                   value = 10,
                                                   min = 5, max = 100,
                                                   step = 5, width = "100%"),
                               shiny::actionButton(inputId = "run_simul", label = "Run Simulation",icon = icon("calendar"))
                               )
                      )),
                      tabPanel(title = "Forecasts", icon=icon("calculator"), value = "res_panel",
                               fluidRow(
                                 column(width = 4,
                                        highcharter::highchartOutput(outputId = "plot_backlog")),
                                 column(width = 4,
                                        highcharter::highchartOutput(outputId = "plot_queue_length")),
                                 column(width = 4,
                                        highcharter::highchartOutput(outputId = "plot_wait_serv"))
                               ),
                               fluidRow(
                                 column(width = 4,
                                        highcharter::highchartOutput(outputId = "plot_time_conf")),
                                 column(width = 4,
                                        highcharter::highchartOutput(outputId = "plot_util_pers")),
                               )
                               )
                      
             )))
                                      